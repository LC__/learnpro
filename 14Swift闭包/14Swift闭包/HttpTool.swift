//
//  HttpTool.swift
//  14Swift闭包
//
//  Created by LC on 16/9/8.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class HttpTool: NSObject {
    
    
    var callBack : ((url : String, info : String)->(Bool))?
    
    //闭包类型：()->()  参数列表->返参类型
    func requestData(callback : (url : String, info : String)->(Bool), urlRe : String) -> Void {
        
        self.callBack = callback
        
        //异步请求网络数据
        dispatch_async(dispatch_get_global_queue(0, 0)) { 
            print("Loading : \(NSThread.currentThread())")
            //异步回到主线程
            dispatch_async(dispatch_get_main_queue(), {
                
                let isLoad = callback(url: urlRe, info: "This is the info from Internet.")

//                while !isLoad {
//                    print("界面更新错误重新请求数据")
//                    isLoad = callback(url: urlRe, info: "This is the info from Internet.")
//                }
                if isLoad {
//                    isOk = true
                    print("界面更新完毕！网络请求结束")
                }
            })
        }
        
        
    }
}
