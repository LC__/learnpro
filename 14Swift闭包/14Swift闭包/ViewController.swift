//
//  ViewController.swift
//  14Swift闭包
//
//  Created by LC on 16/9/8.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var httptool : HttpTool = HttpTool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- wefewqwefw
    
    func loadData() -> Bool {
        var isOk = true
        
        
//        闭包的赋值
//        {
//                (参数) - (返回值) in
//                //代码
//        }
        
        
        //解决循环引用
//        方案一：定义 weak 量 （需要强制解包）
//        weak var weakSelf = self
//        
//        httptool.requestData({ (url, info) -> (Bool) in
//            
//            weakSelf?.view.backgroundColor = UIColor.purpleColor()
//            
//            if info == ""{
//                
//                print("未请求到数据，继续请求")
//                isOk = false
//                return false
//            }
//            
//            print("正在处理数据--\(url):\(info)")
//            print("数据处理完毕，更新界面")
//            isOk = true
//            return true
//            }, urlRe: "www.baudu.com")
        
//        方案二  weak修饰（需要强制解包）
//        httptool.requestData({ [weak self] (url, info) -> (Bool) in
//            
//            self?.view.backgroundColor = UIColor.orangeColor()
//            
//            if info == ""{
//                
//                print("未请求到数据，继续请求")
//                isOk = false
//                return false
//            }
//            
//            print("正在处理数据--\(url):\(info)")
//            print("数据处理完毕，更新界面")
//            isOk = true
//            return true
//            }, urlRe: "www.baudu.com")
        
//        方案三  unowned（不用强制解包）  相当于 _unsafe_unretained:对象销毁的时候会指向原地址（可能也指针）  __weak 对象销毁时自动指向空
        
        httptool.requestData({ [unowned self] (url, info) -> (Bool) in
            
            self.view.backgroundColor = UIColor.orangeColor()
            
            if info == ""{
                
                print("未请求到数据，继续请求")
                isOk = false
                return false
            }
            
            print("正在处理数据--\(url):\(info)")
            print("数据处理完毕，更新界面")
            isOk = true
            return true
            }, urlRe: "www.baudu.com")
        
        return isOk
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        while !loadData() {
            print("失败 继续请求！！")
        }
    }
//    swift 没有dealloc函数 但是有 析构函数
    deinit {
        print("viewcontroller ------- deinit")
    }
}



