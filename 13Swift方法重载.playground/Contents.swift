//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Math {
    
    func sum(num1 num1 : Int, num2 : Int) -> Int {
        return num1 + num2
    }
//    函数的重载 参数的个数不同 参数的类型不同（这一点回合NSObject 的消息机制imp指针冲突）
//    解决方法 1.去掉集成的NSobject 或者 2.方法前加 private 修饰方法(副作用方法外部掉不到😂)
    func sum(num1 num1 : Int, num2 : Int, num3 : Int) -> Int {
        return num1 + num2 + num3
    }
    
    func sum(num1 num1 : Double, num2 : Double) -> Double {
        return num1 + num2
    }
    
}


let math = Math()

math.sum(num1: 2.3, num2: 3.2)
math.sum(num1: 2, num2: 3, num3: 4)
math.sum(num1: 3, num2: 4)
        