//
//  ViewController.swift
//  15Swift懒加载
//
//  Created by LC on 16/9/8.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //数据只会在用到的时候才会加载 而且只会加载一遍
    //这本质是一个闭包 没有参数 只有返回值的闭包其实类似于
    lazy var names : [String] = {
//        ()->[String] in
        print("加载数据")
        return ["LC","GXX","WBB"]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print(names)
        names.append("OKOKOK")
        print(names)
    }
}

