//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//括号省略
//没用非0即真 必须有真假值得结果
//Bool 值变成了 true和false

let a = 20
let flag = a < 10

if flag {
    print("a<10")
}else{
    print("a>=10")
}

let score = 87
if score < 60 {
    print("Not pass")
}else if score < 70{
    print("OK")
}else if score < 80{
    print("Nice")
}else{
    print("666")
}

//三目运算符

let age = 14
var result = age >= 18 ? "OK" : "Not Ok"
print(result)



//guard 新增语法（提高可读性）它就只有为false else

func isOnline(age : Int) -> Bool {
//    if age >= 18 {
//        return true
//    }else{
//        return false
//    }
    guard age >= 18 else{
        return false
    }
    return true
}


isOnline(age)

isOnline(56)


//Switch case
// 可以省略括号
//case 可以不跟case 不会穿透
//default不能省

// 0 女 1男
let sex = 1
switch sex {
case 0:
    print("nv")
case 1:
    print("nan")
default:
    print("NnKnow")
}

//switch case case穿透 fallthrough case可以判断多个条件

switch sex {
case 0,1:
    print("peson")
//case 0:
//    print("nv")
//    fallthrough
//case 1:
//    print("nan")
default :
    print("Unknow")
}

//switch可以判断 浮点 和 字符串

let pai = 3.14

switch pai {
case 3.14:
    print("is pai")
default:
    print("not pai")
}

let m = 20
let n = 25
var resultK : Int


let  opration = "+"

switch opration {
    case "+":
    resultK = m + n
    case "-":
    resultK = m - n
    case "/":
    resultK = m / n
    case "*":
    resultK = m * n
default:
    print("gun")
}

//switch 可以判断区间
let score1 = 88.9
//区间：
//闭区间 0 ~ 10 [0，10] 0...10
//开区间 0 ~ 9 [0，9) 0..<10
switch score1 {
case 0..<60 :
    print("bujige")
case 60...100 :
    print(" Nice")
default:
    print("error")
    
}

