//
//  GlobalTool.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/8.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class GlobalTool: NSObject {

    
}
//全局编译打印
func lcPrint<T>(message: T,
              file: String = #file,
              method: String = #function,
              line: Int = #line)
{
//    需要在buid setting 里面 定义 custom Flag = -D DEBUG
    #if DEBUG
        print("\((file as NSString).lastPathComponent)[\(line)], \(method): \(message)")
    #endif
}

//获取当前命名空间名
func lcGetSpaceName() -> String {
    guard let spaceName =  NSBundle.mainBundle().infoDictionary!["CFBundleExecutable"] as? String else{
        lcPrint("获取命名空间失败")
        return "Error"
    }
    return spaceName
}

//通过字符串获取类
func lcClassFormString(className: String) -> AnyClass? {
    
    let calssNameTrue = lcGetSpaceName() + "." + className
    
    let calzz : AnyClass? = NSClassFromString(calssNameTrue)
     lcPrint(calzz)
    
    return calzz
    
}