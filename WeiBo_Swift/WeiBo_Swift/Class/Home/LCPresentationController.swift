//
//  LCPresentationController.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/12.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

//  自定义转场动画 modal 出来的控制器不会移除原有的控制器而且可以在containerViewWillLayoutSubviews方法中控制modal控制器的尺寸（非自定义会移除，且尺寸和屏幕一样大）
class LCPresentationController: UIPresentationController {
    
    
//    保存菜单的尺寸
    var presentFrame: CGRect = CGRectZero
    
    override init(presentedViewController: UIViewController, presentingViewController: UIViewController) {
        super.init(presentedViewController: presentedViewController, presentingViewController: presentingViewController)
    }
    
    //用于布局转场动画弹出的空间
    override func containerViewWillLayoutSubviews() {
//        containerView 就是容器视图
//        presentedView() 弹出视图

        presentedView()?.frame = presentFrame
        coverBtn.addTarget(self, action: #selector(LCPresentationController.coverClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
//        coverBtn.backgroundColor = UIColor.redColor()
       
        containerView!.insertSubview(coverBtn, atIndex: 0)
        
//        设置遮罩
        
        
    }
    
    private lazy var coverBtn: UIButton = {
        let btn = UIButton()
        btn.frame = UIScreen.mainScreen().bounds
        
        return btn
    }()
    
//    MARK: cover 点击响应
    @objc private func coverClick(sender: UIButton){
        lcPrint("")
//        让菜单消失
        presentedViewController.dismissViewControllerAnimated(true, completion: nil)
    }

}















