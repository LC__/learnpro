//
//  LCPresentationManager.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/13.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

//展现和消失时的 通知
let LCPresentationManagerDidPresented = "LCPresentationManagerDidPresented"
let LCPresentationManagerDidDismissed = "LCPresentationManagerDidDismissed"


//负责控制 返回转场动画的处理对象
class LCPresentationManager: NSObject, UIViewControllerTransitioningDelegate {
    
    //    保存弹出视图控制器的尺寸
    var presentFrame: CGRect = CGRectZero
    
    //是否是展现 页面
    private var isPresent = false
    //MARK: - UIViewControllerTransitioningDelegate
    //该方法用于返回一个负责此转场的动画的对象
    //可以在对象中控制弹出视图的尺寸
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController, sourceViewController source: UIViewController) -> UIPresentationController?{
        let pc = LCPresentationController(presentedViewController: presented, presentingViewController: presenting )
        pc.presentFrame = presentFrame
        return pc
        
    }
    
    //该方法用于返回一个负责转场 如何出现 的对象
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning?{
        lcPrint("show")
        isPresent = true
        // 发送通知 告诉调用者 状态改变了
        NSNotificationCenter.defaultCenter().postNotificationName(LCPresentationManagerDidPresented, object: self)
        return self
    }
    //该方法用于返回一个负责转场 如何消失 的对象
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?{
        lcPrint("out")
        isPresent = false
         NSNotificationCenter.defaultCenter().postNotificationName(LCPresentationManagerDidDismissed, object: self)
        return self
    }
    
}

//负责动画相关 时间 动画细节
extension LCPresentationManager: UIViewControllerAnimatedTransitioning {
    //MARK: - UIViewControllerAnimatedTransitioning
    
    //告诉系统 展现和时间的 时长  暂时用不上
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval{
        return 0.3
    }
    
    //用于控制 modal 如何 展现 和 消失 展现和消失都会调用此方法  只要我们实现了这个方法，那么系统就不会有默认的 modal动画了（从下至上弹出动画）所有的动画都需要我们自己实现 包括需要展现的视图也需要我们自己添加到容器视图上
    
    //    transitionContext 所有动画需要的东西都保存在这个上下文中
    func animateTransition(transitionContext: UIViewControllerContextTransitioning){
        
//        let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
//        let fromVC  = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        
        if isPresent {
            
            willPresentedController(transitionContext)
            
        }else{
            
            willDismissedController(transitionContext)
            
        }
    }
    
    private func willPresentedController(transitionContext: UIViewControllerContextTransitioning ){
    
        //        1.获取需要弹出的视图  UITransitionContextToViewControllerKey, and
        // UITransitionContextFromViewControllerKey.
        
        
        //        UITransitionContextFromViewKey, and UITransitionContextToViewKey  toView 就是 toVC 对应的view fromView就是fromVC对应的view
        guard let toView = transitionContext.viewForKey(UITransitionContextToViewKey) else {
            return
        }
        
        //        2.将视图添加到 containerView 上
        transitionContext.containerView()?.addSubview(toView)
        //        3.执行动画
        //        缩放 一开始的时候 宽高的缩放比
        toView.alpha = 0.0
        toView.transform = CGAffineTransformMakeScale(1, 0.0)
        //        设置锚点 从layer的那个比例位置 缩放
        toView.layer.anchorPoint = CGPointMake(0.5, 0)
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            toView.transform = CGAffineTransformIdentity
            toView.alpha = 1.0
            //            _表示忽略这个值
        }) { (_) in
            //                自定转场动画在执行完之后一定要告诉系统动画完毕
            transitionContext.completeTransition(true)
        }

        
    }
    
    private func willDismissedController(transitionContext: UIViewControllerContextTransitioning ){
        //            1.拿到需要消失的视图
        guard let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey) else {
            return
        }
        //            2.执行动画让它消失
        
        //            fromView.transform = CGAffineTransformMakeScale(1, 1)
        fromView.layer.anchorPoint = CGPointMake(0.5, 0)
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            //                突然消失的原因是cgfloat不准确 导致无法执行动画 将cgfloat 改成一个很小的值
            fromView.transform = CGAffineTransformMakeScale(1.0, 0.000001)
            fromView.alpha = 0.0
            }, completion: { (_) in
                transitionContext.completeTransition(true)
//                type
        })
        
    }
}


