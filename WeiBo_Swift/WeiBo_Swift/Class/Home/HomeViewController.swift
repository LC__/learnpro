//
//  HomeViewController.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/8.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class HomeViewController: BaseTableViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // 判断用户是否登录
        if !isLogin {
            //何止访客视图
            visitorView?.setUpVistorInfo(nil, title: "关注一些人，回这里看看有什么惊喜")
            return
        }
        
        setUpNav()
        
        //注册通知
//        这里面的object 是说 是谁发的通知
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.titleChange), name: LCPresentationManagerDidPresented, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.titleChange), name: LCPresentationManagerDidDismissed, object: nil)
        
        
    }
  
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
        lcPrint("移除通知，并销毁对象")
    }
    

//    MARK: - navTitleView 监听事件
    private func setUpNav() {
        //        添加导航条按钮
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(imgName: "navigationbar_friendattention", target: self, selector: #selector(HomeViewController.leftBtnClick(_:)))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(imgName: "navigationbar_pop", target: self, selector: #selector(HomeViewController.rightBtnClick(_:)))
        
        
        
        //        titleview
        navigationItem.titleView = titleB
    }
//    MARK: - 标题按钮
    private lazy var titleB: TitleButton = {
        let titleB = TitleButton()
        titleB.setTitle("天空中最不亮的星", forState: UIControlState.Normal)
        
        
        titleB.addTarget(self, action: #selector(HomeViewController.titleBtnClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        return titleB
    }()
    
//    MARK: - 转场动画管理
    private lazy var animatorManager: LCPresentationManager = {
       let manager = LCPresentationManager()
        manager.presentFrame = CGRect(x: 100, y: 55, width: 200, height: 200)
       return manager
        
    }()
    
//    MARK: - title 监听事件:
    @objc private func titleBtnClick(sender: UIButton){
        lcPrint("title")
//        exit(0)
        //修改按钮状态
//        sender.selected = !sender.selected
        //创建显示菜单ver
        let sb  = UIStoryboard(name: "Popover", bundle: nil)
        let menuView = sb.instantiateInitialViewController()
        guard let menuViewVc = menuView else{
            lcPrint("")
            return
        }
        //自定义转场动画
//        1.自定义动画
        
//        2.设置代理
        menuView?.transitioningDelegate = animatorManager
//        3.设置样式
        menuView?.modalPresentationStyle = UIModalPresentationStyle.Custom
        
        presentViewController(menuViewVc, animated: true, completion: nil)
        
    }

    
//    MARK: - nav左右Btn 监听事件
    @objc private func leftBtnClick(sender: UIButton){
        lcPrint("left")
    }
    
    @objc private func rightBtnClick(sender: UIButton){
        lcPrint("right")
//        1.穿件二维码扫描控制器
        let sb = UIStoryboard(name: "QRCode", bundle: nil)
        let qrView = sb.instantiateInitialViewController()!
//        2.弹出视图控制器
        presentViewController(qrView, animated: true, completion: nil)
        
        
    }
//    MARK: - title 箭头变化事件
    @objc private func titleChange(){
        titleB.selected = !titleB.selected
    }
    
}

// MARK: - Table view data source
extension HomeViewController{
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
}
