//
//  QRCodeViewController.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/13.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit
import AVFoundation

class QRCodeViewController: UIViewController {
//    冲击波视图
    @IBOutlet weak var scanWave: UIImageView!

//    二维码扫面结果显示Label
    @IBOutlet weak var laScanResult: UILabel!
//    扫描框的高度
    @IBOutlet weak var scanHeight: NSLayoutConstraint!
//    冲击波的顶部约束
    @IBOutlet weak var scanLineCons: NSLayoutConstraint!
//    底部工具条
    @IBOutlet weak var customTabbar: UITabBar!
    override func viewDidLoad() {
        super.viewDidLoad()

//    设置默认的选中
        customTabbar.selectedItem = customTabbar.items?.first
        
//        设置监听，监听底部工具点击
        customTabbar.delegate = self
        
//        开始扫描二维码
        scanQRCode()
        
    }
//    MARK: - 输入输出对象进入会话 开时扫描二维码逻辑
    private func scanQRCode() {
//        1.判断输入能否添加到会话中
        if !session.canAddInput(inputObj) {
            return
        }
        
//        2.判断输出能否加入到会话中
        if !session.canAddOutput(outputObj) {
            return
        }
//        3.添加输入输出到会话
        session.addInput(inputObj)
        session.addOutput(outputObj)
    
//        4.设置输出能够解析的数据类型
//        注意点：设置数据类型一定要在数处对象添加到会话之后 才能设置
//        为了简单，直接添加了所有类型
        outputObj.metadataObjectTypes = outputObj.availableMetadataObjectTypes
//        5.设置监听 用以监听解析到的数据类型
//        在主线程实现协议
        outputObj.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
//        6.添加预览图层
//        view.backgroundColor = UIColor(white: 0.4, alpha: 0.9)
        view.layer.insertSublayer(preViewLayer, atIndex: 0)
        preViewLayer.frame = view.bounds
        
        view.layer.addSublayer(containerLayer)
        containerLayer.frame = view.bounds
//        7.开始扫描
        session.startRunning()
    }

//    MARK: - 左右item点击事件
    @IBAction func leftItemClick(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        startScan()
    }

 
    @IBAction func rightItemClick(sender: AnyObject) {
        
    }
//    动画
    private func startScan() {
        //        设置动画
        //        设置处置
        scanLineCons.constant = -scanHeight.constant
        view.layoutIfNeeded()
        
        UIView.animateWithDuration(2.0) {
            UIView.setAnimationRepeatCount(MAXFLOAT)
            //        swift 不推荐写self 关键字，除非为了区分两个属性，或者实在闭包党当中调用本类属性
            //            优点：提醒程序员此时是否会引起循环引用
            self.scanLineCons.constant = self.scanHeight.constant
            self.view.layoutIfNeeded()
        }

    }
    
//    MARK: - 懒加载 QR扫描相关-
//    输入对象
    private lazy var inputObj: AVCaptureDeviceInput? = {
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        let result = try? AVCaptureDeviceInput(device: device)
        return result
    }()
//    会话
    private lazy var session: AVCaptureSession = {
        return AVCaptureSession()
    }()
//    输出对象
    private lazy var outputObj: AVCaptureMetadataOutput = {
        return AVCaptureMetadataOutput()
    }()
//    预览图层
    private lazy var preViewLayer: AVCaptureVideoPreviewLayer = {
        let layer = AVCaptureVideoPreviewLayer(session: self.session)
        return layer
    }()
//    描边图层
    private lazy var containerLayer: CALayer = {
    return CALayer()
    }()
    
}
//MARK: - UITabBarDelegate
extension QRCodeViewController: UITabBarDelegate{
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        lcPrint(item.tag)
        
        scanHeight.constant = (item.tag == 1) ? 150 : 300
        
        //移除动画，重新开启动画
        scanWave.layer.removeAllAnimations()
        startScan()
        
    }
}
//MARK: - AVCaptureMetadataOutputObjectsDelegate  扫描二维码相关
extension QRCodeViewController: AVCaptureMetadataOutputObjectsDelegate{
//    只要扫描到结果就会调用
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!){
        lcPrint(metadataObjects.last!)
//        1.显示结果
        laScanResult.text = metadataObjects.last?.stringValue
//        只要执行了扫描就清空所有的描边
        clearLayers()
        
//        这里可以获取正在扫描的二维码的位置信息,通过预览图层可以转化成为我们识别的的坐标
        
        guard let item = (metadataObjects.last as? AVMetadataObject) else{
            return
        }
        let objc = preViewLayer.transformedMetadataObjectForMetadataObject(item)
        
        lcPrint((objc as! AVMetadataMachineReadableCodeObject).corners)
        
//        2.对二维码进行描边
        drawLines(objc as! AVMetadataMachineReadableCodeObject)
        
        
    }
    private func drawLines(objc: AVMetadataMachineReadableCodeObject) {
        
//        0.安全校验
        guard let array = objc.corners else{
            return
        }
        
//        1.创建图片，用于保存绘制的矩形
        let layer = CAShapeLayer()
        layer.lineWidth = 2
        layer.strokeColor = UIColor.cyanColor().CGColor
        layer.fillColor = UIColor.clearColor().CGColor
        
//        2.创建UIBezierPath，绘制矩形
//        let path = UIBezierPath(rect: CGRect(x: 100, y: 100, width: 200, height: 200))
        let path = UIBezierPath()
        var index = 0
        var point = CGPointZero
        
        CGPointMakeWithDictionaryRepresentation((array[index] as!CFDictionary), &point)
        index += 1
        
//        1)将起点移动到某一个点
        path.moveToPoint(point)
        
        
        
//        2)连接其他线段
        while index < array.count {
            CGPointMakeWithDictionaryRepresentation((array[index] as!CFDictionary), &point)
            index += 1
            path.addLineToPoint(point)
            
        }
        
//        3）关闭路径
        path.closePath()
        
        
        layer.path = path.CGPath

//        3.将用于保存举行的图层添加到界面上
        containerLayer.addSublayer(layer)
    }
    
//    清空描边
    private func clearLayers()  {
        guard let subLayers = containerLayer.sublayers else{
            return
        }
        for layer in subLayers {
            layer.removeFromSuperlayer()
        }
    }
}
