//
//  BaseTableViewController.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/9.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

//通知：层级结构深
//代理：父子 方法较多
//block：父子 一个方法
class BaseTableViewController: UITableViewController {

    
    //默认登录标记为空
    var isLogin = false
    
    var visitorView: VisitorView?

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func loadView() {
        //判断用户是否登录
        
        //处理获取登录状态
        isLogin = true
        
        isLogin ? super.loadView() : setupVistorView()

    }
    
    //MARK: - 内部方法
    private func setupVistorView(){
        visitorView = VisitorView.visitorView()
//        visitorView?.delegate = self
//        也可以在base里面给btn添加事件
        visitorView?.btnLoad.addTarget(self, action: #selector(BaseTableViewController.loginAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        visitorView?.btnRegist.addTarget(self, action: #selector(BaseTableViewController.registAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        view = visitorView
        
//        添加导航栏按钮
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(BaseTableViewController.registAction(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(BaseTableViewController.loginAction(_:)))

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
  
    }

   
    
//    MARK: -mark 登录和注册的时间监听
    @objc func loginAction(sender: UIButton)   {
        lcPrint("load")
        
    }
    @objc func registAction(sender: UIButton)  {
        lcPrint("regist")
    }
    

}
    // MARK: - Table view data source
extension BaseTableViewController {
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
}

extension BaseTableViewController: VisitorViewDlegate{
    func vistorViewDidClickLoginBtn(vistorView: VisitorView){
        lcPrint("Login")
    }
    
    func vistorViewDidClickRegisterBtn(vistorView: VisitorView){
        lcPrint("regist")
    }
}
