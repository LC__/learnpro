//
//  VisitorView.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/9.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

protocol VisitorViewDlegate: NSObjectProtocol {
//    默认协议中的方法必须实现
    func vistorViewDidClickLoginBtn(vistorView: VisitorView)
    
    func vistorViewDidClickRegisterBtn(vistorView: VisitorView)
}

class VisitorView: UIView {
    weak var delegate: VisitorViewDlegate?
    
    @IBOutlet weak var imgTurnplate: UIImageView!


    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var btnLoad: UIButton!
    
    @IBOutlet weak var laInfo: UILabel!
    
    @IBOutlet weak var btnRegist: UIButton!
    
//    需要显示的图标， 需要显示的标题
    func setUpVistorInfo(imgName: String?, title: String) {
        laInfo.text = title
        
        guard let name = imgName else {
            //是首页
            //转转盘
            startAnimation()
            return
        }
        imgTurnplate.hidden = true
        imgIcon.image = UIImage(named: name)
    }
    class func visitorView() -> VisitorView{
        return NSBundle.mainBundle().loadNibNamed("VisitorView", owner: nil, options: nil).first as! VisitorView
    }
    private func startAnimation(){
//        1.创建动画
        let animation = CABasicAnimation(keyPath: "transform.rotation")
//        2.设置属性
        animation.toValue = 2 * M_PI
        animation.duration = 5.0
        animation.repeatCount = MAXFLOAT
//        默认情况，视图消失时会移除动画，设置使其不要移除动画
        animation.removedOnCompletion = false
//        3.添加到图层
        imgTurnplate.layer.addAnimation(animation, forKey: nil)
    }

    @IBAction func loadClick(sender: AnyObject) {
//        lcPrint("load")
        delegate?.vistorViewDidClickLoginBtn(self)
    }
    @IBAction func registClick(sender: AnyObject) {
//        lcPrint("regist")
        delegate?.vistorViewDidClickRegisterBtn(self)
    }
}
