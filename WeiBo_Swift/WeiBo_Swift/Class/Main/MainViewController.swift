//
//  MainViewController.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/8.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    
//    MARK: - 生命周期函数
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        storyboard 中已经完成了
//        addChildViewControllers()
        
//        viewDidload 中vc的view加载完毕但是子视图不一定加载完毕
       
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
//        添加加号按钮
        let rect = composeButton.frame
        let width = tabBar.bounds.width / CGFloat(childViewControllers.count)
        composeButton.frame = CGRect(x: width * 2, y: 0, width: width, height: rect.height)
        tabBar.addSubview(composeButton)
    }

//    MARK: - 添加子控制器
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
     func addChildViewControllers() {
        tabBar.tintColor = UIColor.orangeColor()

//        通过json 数据动态创建控制器
        guard let filePath = NSBundle.mainBundle().pathForResource("MainVCSettings.json", ofType: nil) else{
            lcPrint("json 不存在！")
            return
        }
        lcPrint(filePath)
        
        
        guard let data = NSData(contentsOfFile: filePath) else{
            lcPrint("加载json失败")
            return
        }
        
//        转成可变的对象
//        Swift 中和 OC 不太一样 OC中是传入错误指针，而Swift 使用的是一场处理机制
//        1.throw -> try catch 处理  
//        2.do{ try func } catch { }
//        3.try 是正常处理异常 也就是使用 do catch
//        4.try！ 告诉编译器 一定不会有异常 也就是 不通过 do catch 来处理(不推荐，有错会崩)没有异常会给一个确定的值
//        5.try？ 告诉编译器 可能不会有异常 如果没错系统会走动包装成一个可选类型给你，如果有错系统会给一个nil 不适用 do catch 块
        
        do{
            let objc = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves) as! [[String: AnyObject]]
//            lcPrint(objc)
            for dict in objc {
                lcPrint(dict)
                let title = dict["title"] as? String
                let vcName = dict["vcName"] as? String
                let imageName = dict["imageName"] as? String
//                let title = dict["title"]
                addChildViewController(vcName, title: title, imageName: imageName)
                
                
                
                
            }
        }catch{
//            若try的方法发生错误了就会执行 catch 块
//                    创建固定的控制器
            
                addChildViewController("HomeViewController", title: "首页", imageName: "tabbar_home")
                addChildViewController("MessageViewController", title: "消息", imageName: "tabbar_message_center")
                addChildViewController("NullViewController", title: "", imageName: "")
                addChildViewController("DiscoverViewController", title: "发现", imageName: "tabbar_discover")
                addChildViewController("ProfileViewController", title: "我的", imageName: "tabbar_profile")
            
            
        }
        
        
        
    }
    
//    重载了子类集成了父类的方法
//     func addChildViewController(childController: UIViewController, title: String, imageName: String) {
//        
////        Swift中增加了 命名空间 这一概念 不同项目中的命名空间是不一样的 默认为自己当前项目的名称  作用 避免命名重复   
////        正是因为 Swift 可以通过 命名空间来结局命名重复 这个问题 尽量使用 cocoapods来集成第三方框架，
////        所以通过字符串 创建类的时候需要在前面 添加命名空间
//        
//     lcPrint(childController)
//        
////        childController.tabBarItem.title = title
//        //        let image = UIImage() 设置图片的模式使得图片颜色
//        childController.tabBarItem.image = UIImage(named: imageName)
//        childController.tabBarItem.selectedImage = UIImage(named: "\(imageName)_highlighted")
//        //        一并设置tabbar标题和图片的颜色
////        设置nav和tabbar的title
//        childController.title = title
//        let nav = UINavigationController(rootViewController: childController)
//        
//        addChildViewController(nav)
//    }
    
//    由于可选绑定太多，Swift代码使用if 会嵌套很多层次，于是可以使用guard使得 嵌套 变得简单。
    
    func addChildViewController(childControllerName: String?, title: String?, imageName: String?) {
//        通过字符串 创建类
//        需要动态获取 命名空间
//        通过字典的键值去除的值是 anyobject？ 可能没有值
        
        guard let vcName = childControllerName else {
            lcPrint("无控制器类名")
            return
        }
        
        guard let vcTitle = title else {
            lcPrint("无控制器title")
            return
        }
        
        guard let vcImgName = imageName else {
            lcPrint("无控制器image")
            return
        }
        
        let calzz : AnyClass? = lcClassFormString(vcName)
        
        lcPrint(calzz)
        
//            在Swift中 如果需呀使用 class 来创建对象，必须告诉编译器它的真实类型
        guard let typeClass = calzz as? UITableViewController.Type else{
            lcPrint("class 不是UITableViewController.type");
            return
        }
        
        //            通过类来创建对象
        let childController = typeClass.init()
        
        lcPrint(childController)
        
        //        childController.tabBarItem.title = title
        //        let image = UIImage() 设置图片的模式使得图片颜色
        childController.tabBarItem.image = UIImage(named: vcImgName)
        childController.tabBarItem.selectedImage = UIImage(named: "\(vcImgName)_highlighted")
        //        一并设置tabbar标题和图片的颜色
        //        设置nav和tabbar的title
        childController.title = vcTitle
        let nav = UINavigationController(rootViewController: childController)
        
        addChildViewController(nav)
    

    }
//    MARK: - 懒加载 加号按钮
    lazy var composeButton: UIButton = {
        () -> UIButton in
//        let btn = UIButton()
//        
////        前景图片
//        btn.setImage(UIImage(named: "tabbar_compose_icon_add") , forState: UIControlState.Normal)
//        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted") , forState: UIControlState.Highlighted)
////        背景图片
//        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: UIControlState.Normal)
//        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
////        事件
        
        let btn = UIButton(imageName: "tabbar_compose_icon_add", backImageName: "tabbar_compose_button")
        btn.addTarget(self, action:#selector(MainViewController.compseBtnClick(_:)), forControlEvents: UIControlEvents.TouchUpInside)
//        尺寸  和他的内容尺寸一样大
        btn.sizeToFit()
        return btn
    }()
    
//    将按钮的点击事件设置成private那么他只能在当前类内使用，而点击事件是有运行循环来调用的，它就调用不到了，所以会报错。 oc 是动态派发获取（运行时机制）的  而 Swift 不一样，它把所有东西都要在编译时解决所有问题（运行快！），相当于没有运行时机制。
//    为了解决这个问题，可以使用 @objc 关键字，这样这个方法就是支持动态派发（运行时机制）了
//    相当于 Swift 和 oc 的混编
    @objc private func compseBtnClick(sender: UIButton) {
        lcPrint("Btn点击事件")
        lcPrint(sender)
    }

}
