//
//  UIBarButtonItem+selectedImg.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/12.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

extension UIBarButtonItem{
//    便利构造器
    convenience init(imgName: String, target: AnyObject?, selector: Selector) {
        
        let item = UIButton()
        item.setImage(UIImage(named: imgName), forState: UIControlState.Normal)
        item.setImage(UIImage(named: "\(imgName)_highlighted"), forState: UIControlState.Highlighted)
        item.sizeToFit()
        item.addTarget(target, action: selector, forControlEvents: UIControlEvents.TouchUpInside)
        
        self.init(customView: item)
        
    }
}
