//
//  TitleButton.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/12.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class TitleButton: UIButton {
    
//    重写父类的方法(与oc差不多)
//    类扩展重写init方法的时候需要加 convenience 
//    两者都是重写，但是不一样
    override  init(frame: CGRect) {
        super.init(frame: frame)
        setUpUI()
    }
    
//    通过xib/sb创建的时候
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpUI()
    }
    
    private func setUpUI() {
        setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        setImage(UIImage(named: "navigationbar_arrow_down"), forState: UIControlState.Normal)
        setImage(UIImage(named: "navigationbar_arrow_up"), forState: UIControlState.Selected)
        sizeToFit()
    
    }
    
//    自定义 标题 的frame
//    override func titleRectForContentRect(contentRect: CGRect) -> CGRect {
//        return CGRectZero
//    }
////    自定义 图片 的frame
//    override func imageRectForContentRect(contentRect: CGRect) -> CGRect {
//        return CGRectZero
//    }
//    自定义 所有子视图 的frame 这个方法会调用两次
    override func setTitle(title: String?, forState state: UIControlState) {
        
//        ?? () 用于判断？？前的东西是否为空 若为空则返回（）的值 否则不执行
        
        super.setTitle((title ?? "") + "  ", forState: state)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        设置控件的frame偏移量
//        titleLabel?.frame.offsetInPlace(dx: -imageView!.bounds.width, dy: 0)
//        imageView?.frame.offsetInPlace(dx: titleLabel!.bounds.width, dy: 0)
        
//        swift允许直接修改对象结构体的的值
        titleLabel?.frame.origin.x = 0
        imageView?.frame.origin.x = titleLabel!.bounds.width
    }
}
