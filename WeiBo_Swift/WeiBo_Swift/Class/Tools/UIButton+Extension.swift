//
//  UIButton+Extension.swift
//  WeiBo_Swift
//
//  Created by LC on 16/9/9.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

extension UIButton{
//    如果构造方法中没有 convenience，代表这是一个初始化构造方法（指定构造方法）
//    如果有代表这是一个便利构造方法
//    指定构造方法中必须对类的所有属性进行初始化（语法规定，类的所有属性必须有初始化） 而便利的构造方法则不用这样，但是便利构造方法必须调用指定构造方法
//    系统类由于属性未知 所以给其写构造方法时需要写便利构造方法

    convenience init(imageName: String, backImageName: String) {
        self.init()
        setImage(UIImage(named: imageName) , forState: UIControlState.Normal)
        setImage(UIImage(named: "\(imageName)_highlighted") , forState: UIControlState.Highlighted)
        //        背景图片
        setBackgroundImage(UIImage(named: backImageName), forState: UIControlState.Normal)
        setBackgroundImage(UIImage(named: "\(backImageName)_highlighted"), forState: UIControlState.Highlighted)
    }
}
