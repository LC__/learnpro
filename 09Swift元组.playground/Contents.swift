//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//就是（） 经常作为函数的返回值 很简单地返回多个参数

let b = (name : "LC", sex : "Man", age : 34, friend: "GXX")
let item = ("LC", "Man", 34, "GXX", "F")

let  (name, sex, age, friend) = ( "LC", "Man", 34, "GXX")

print(b.age)

let error = (404,"Not Found")
error.0
error.1

let error1 = (errorCode : 404,errorInfo : "Not Found")
error1.errorCode
error1.errorInfo

let (errorCode1 , errorInfo1) = (404, "Not Found")
errorCode1
errorInfo1