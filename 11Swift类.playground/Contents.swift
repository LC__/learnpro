//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//class 类名:父类 {
//    //属性方法
//}

//定义类时可以没有父类 这样的话 他是 rootClass
//通常我们可以继承自 NSObject （但是它不是 oc 的那个NSObject 😂）



class Stufent: NSObject {
    //存储属性  最简单 常见 可是提供默认值 或者 在初始化方法
        //类型若是 结构体或类 的时候 最好定义为 可选类型 进本类型 直接初始化为 0，0.0
    
    var name : String?
    var age : Int = 0
    var chineseScore : Double = 0.0
    var mathScore : Double = 0.0
    
    //计算属性 不用于存储属性值 一般提供get方法（set可选） 如果计算属性只有 get方法可以省略get{}关键字
    var averageScore : Double {
//        get{
            return (chineseScore + mathScore) / 2
//        }
        //set 可选 并且在set方法(一般没用)中 有一个系统表示符newValue，用于记录外界传入值
//        set{
//            print(newValue)
//            self.averageScore = newValue
//        }
    }
    
    //类属性
    static var courseCount : Int = 0
    
    
}

let stu = Stufent()
//类属性复制
Stufent.courseCount = 2

stu.name = "LC"
stu.age = 33
stu.chineseScore = 89
stu.mathScore = 99.5


stu.averageScore



//监听属性的改变  一般来说是对于存储属性


class Person: NSObject {
    var name : String? {
        //这不是 set 方法 newValue  也可以自定义名称
        //把newvalue 改名成了 new
        willSet(new) {
            if new == "LC" {
                print(name)
            }else{
                print("不是本人")
            }
            
        }
        didSet {  //oldValue
            print(name)
            print(oldValue)
        }
    }
    var age : Int = 0
    
}
let p = Person()

p.name = "Ll"
p.age = 18

//OC 监听属性的改变：重写Set方法  可以拦截属性的改变
//Swift 提供属性监听器















