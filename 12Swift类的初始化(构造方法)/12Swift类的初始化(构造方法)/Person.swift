//
//  Person.swift
//  12Swift类的初始化(构造方法)
//
//  Created by LC on 16/9/7.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class Person: NSObject {
    var name : String?
    var age : Int = 0
    
    override init() {
        print("对象来了")
//        重写的init方法中 系统会自动调用super.init()
    }
    //默认 也都是 外部参数不用写外部参数名字了
    init(name: String, age : Int) {
        self.name = name
        self.age = age
    }
//    类型的处理
//    从anyObject 转成 string/Int as!/?  (!?的区别是类型是否可选) 转成 oc 类 直接用 as
    init(infoDic:[String : AnyObject]){
        name = infoDic["name"] as? String
        age = infoDic["age"] as! Int
    }
}
