//
//  ViewController.swift
//  12Swift类的初始化(构造方法)
//
//  Created by LC on 16/9/7.
//  Copyright © 2016年 LC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("click")
//        let p = Person()
//        p.name = "LC"
//        p.age = 18
//        print("\(p.name!),\(p.age)")
        
        let p = Person(name: "LC", age: 18)
        
        let dic = ["name" : "GXX" , "age" : 18]
        
        let p1 = Person(infoDic: dic)
        
        print("\(p.name!),\(p.age)")
        
        print("\(p1.name!),\(p1.age)")
        
        
        
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

