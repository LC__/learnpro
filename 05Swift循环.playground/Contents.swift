//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


//------------------for---------------

//写法1
for (var i = 0; i < 10; i++){
    print(i)
}

//写法2 区间写法
for i in 0..<10 {
    print(i)
}

//写法 3 没有用到 i 值
for _ in 0..<10{
    print("nihao")
}


//------------------while---------------


var a = 20

while a < 30 {
    a += 1
    print(a)
}



//------------------do while---------------
var b = 30

repeat  {
    b -= 1
    print(b)
}while b>10