//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//let a : Int = 20
//let b : Double = 3.44

//Swift的 类型推导
//如果一个量 在定义的时候有直接赋值，可以根据这个值来确定量的类型 这时 类型和冒号可以省略。
var a = 20
//a = 1.44

var b = 3.14
//在swift中进行运算必须保证类型是一直的 否则无法进行运算 因为Swift中没有隐式转换，需要显示转换 类型（）。
//let c = Int(b) + a
//print(c)

var d = Double(a) + b
d = Double(a) * b
        