//: Playground - noun: a place where people can play

import UIKit

//函数可以重载！！！！！！！

var str = "Hello, playground"

func show()  {
    print("no para no return")
}

func show(_: Int) -> Void {
    print("has para no return")
}
//
func show1() -> Int {
    print("no para has return")
    return 1
}

func show1(num1: Int,num2: Int) -> Int {
    print("has para has return")
    return 2
}


show()

show(2)

show1()

show1(2, num2: 1)

//函数的局部参数名和外部参数名的控制

func text(x: Int, y: Int, z: Int) -> Void {
    print("point is (\(x),\(y),\(z))")
}

func text1(x x: Int, y: Int, z: Int) -> Void {
    print("point is (\(x),\(y),\(z))")
}

func text2(x: Int, _ y: Int, _ z: Int) -> Void {
    print("point is (\(x),\(y),\(z))")
}

func text3(xx x: Int, yy y: Int, zz z: Int) -> Void {
    print("point is (\(x),\(y),\(z))")
}

text(1, y: 2, z: 3)

text1(x: 3, y: 3, z: 3)

text2(4, 5, 6)

text3(xx: 1, yy: 2, zz: 345334)

