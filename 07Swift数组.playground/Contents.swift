//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//swift 是一个泛型集合

//let 修饰不可变数组  var修饰可变数组 （swift中集合类型是结构体不是类）

//1.数组定义
//let nameList : Array<String> = ["i","love","you"]
//let nameList1 : [String] = ["you","love","me"]
//let nameList2 = ["love",1,"sdf"]
//var names1 : [String] = Array()
//var names2 : [String] = [String]()

//2.增删查改
//swift 开发中可以使用 anyobject代替 nsobject
var names : [String] = ["i","love","you","4","ever"]

names.append("4Ever")
print(names)

names.removeAtIndex(0)
print(names)

names[0] = "LC"
print(names)

let name = names[3]
print(name)

//3.数组遍历
for i in 0..<names.count {
    print(names[i])
}

for item in names {
    print(item)
}

for item in names[0..<3] {
    print(item)
}

for i in 0..<3 {
    print(names[i])
}

//4.数组合并
let arr1 = ["why","wefwe",45]

let arr2 = ["dsfds","dsfsd",12]

let arr3 = arr1 + arr2

let names3 = ["dsfsad","dsfas","wfw"]
let ages3 = [12, 23, 45]

var com = [AnyObject]()

for item in names3 {
    com.append(item)
}

for item in ages3 {
    com.append(item)
}

print(com)

//nameList[1]

