//: Playground - noun: a place where people can play

import UIKit


var str = "Hello, playground"
//string 是一个结构体  NSString 是一个是一个对象性能差一点  
//string 支持直接遍历  
//swift 提供string 和 NSString 之间的无缝转换


//字符串的遍历

for c in str.characters {
    print(c)
}

//字符串拼接

let str1 = "dffwefwefw"
var str2 = "hhhhhh"

str2 = str1 + str2

//字符串类型快速转换
let age = 18
let height = 1.88

let str3 = "age is \(age), height is \(height)"

//字符串格式化

let min = 2
let second = 3
let timeStr = String(format: "%02d:%02d",min,second)


//字符串截取
let urlStr = "www.baidu.com"
let urlStrNS = (urlStr as NSString)

//string 的 方法很日狗（index 难以创建）可以直接使用你nsstring的方法来截取 然后进行转换

var urlhead = urlStrNS.substringToIndex(3)
var urlbody = urlStrNS.substringWithRange(NSMakeRange(4, urlStrNS.length-8))
var urltail = urlStrNS.substringFromIndex(urlStrNS.length-3)

urlhead = urlStr.substringToIndex(urlStr.startIndex.advancedBy(3))
urlbody = urlStr.substringWithRange(Range(urlStr.startIndex.advancedBy(4)..<urlStr.endIndex.advancedBy(-4)))
urltail = urlStr.substringFromIndex(urlStr.endIndex.advancedBy(-3))



