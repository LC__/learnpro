//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//let修饰不可变字典 var修饰可变字典

//1.定义字典

//let dic : [String : AnyObject] = ["1" : "dsfds", "2" : "werqwefew", "3": "ewrwefq"]
//let dic1 : Dictionary<String , AnyObject> = ["1" : "dsfds", "2" : "werqwefew", "3": "ewrwefq"]
//let dic2 = ["1":"wfcdc" , "wdcfdfac":"ewdwfqwe" , "sdfad":12]
//
//var muDic = [String : AnyObject]()
//var muDic1 = Dictionary<String,AnyObject>()
//var muDic2 = ["1":"wfcdc" , "wdcfdfac":"ewdwfqwe" , "sdfad":12]
//2.增删查改
var mDict = [String : AnyObject]()

mDict["1"] = 34
mDict["2"] = 89
mDict["3"] = 9.00
mDict["age"] = "34"

print(mDict)

mDict.removeValueForKey("1")
print(mDict)

//通过该方式修改如没有对应键值则会创建新的键值对
mDict["1"] = 44
print(mDict)

mDict["3"]

//3.字典遍历

for key in mDict.keys {
    print(key)
}

for value in mDict.values {
    print(value)
}

for (key,value) in mDict {
    print(key)
    print(value)
}

for item in mDict{
    print(item.0)
    print(item.1)
}


//4.字典合并 字典不能加减运算

let dic11 = ["name" : "lc","age":13]
var dic12 = ["phoneNum" : "11234324", "height" : 1.88]
for (key1,value1) in dic11 {
    dic12[key1] = value1
}


