//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var age : Int = 20
age = 25

let π :Double = 3.14
//π = 5.33

//1>优先使用常量 需要修改之时在修改为var
//2>常量的意思是 指针指向的对象不能修改。
//在swift创建对象
let view : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 240))
view.backgroundColor = UIColor.redColor()
//swift使用枚举需要用枚举名 . 具体类型
let btn : UIButton = UIButton(type: UIButtonType.ContactAdd)
btn.center = CGPoint(x: 50, y: 40)
view.addSubview(btn)