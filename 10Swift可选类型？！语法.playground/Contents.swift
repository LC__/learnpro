//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//在OC一个变量暂时不是用的时候，暂时可以赋值为nil 或者 0
//但是swift 空 是一种类型， 例如不能把一个空类型辅助给数组类型（类型不匹配😂，有病的优雅设定）
//可选类型（对，他也是一种类型和数组一样）有两种取值（有值 ， 没值）它一边针对变量var



//1.定义可选类型
var name : Optional<String>
var name1 = String?()

//可选类型赋值  其实这个地方 类型不匹配，它把 String类型 给了 可选类型 (编译器在这个地方吧字符串类型转成了字符串，唯一的隐示类型转换)
name = "LC"
print(name)

//取出可选类型中的真实类型  强制解包
print(name!)


//语法糖定义
//let phoneNum : Optional<String>
var phoneNum : String? = nil


phoneNum = "+86 234123423112"
print(phoneNum)

//强制解包之前需要判断 若有值 再进行解包
if phoneNum != nil {
    let info = "my phoneNum is " + phoneNum!
}

//可选绑定
//1>如果有值 则会解包 如果无值会跳过语句块(这不是if语法)

//用这种判断方式
if let tempPhoneNum = phoneNum {
   print(tempPhoneNum)
}



class Person {
    var name : Optional<String>
    
}

//-----------------------使用场景

let urlStr = "http://www.baidu.com/"
let url : NSURL? = NSURL(string: urlStr)

//若是urlstr中有中文，穿件出的url为空 下一步穿件 request 程序就会崩溃。这样的做法会保护应用程序
var urlRequest = NSURLRequest()


if let tempUrl = url {
     urlRequest = NSURLRequest(URL: tempUrl)
}




        